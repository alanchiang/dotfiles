set number
colorscheme desert
syntax on
"set guifont=Courier_New:h9:cANSI
set backspace=2
set ruler
set tabstop=4
set showmatch
set hlsearch
set incsearch
set ignorecase
set infercase
set shiftwidth=4
set cmdheight=1
set history=50
set showcmd
set laststatus=2
set nocompatible
set cursorline
set smartindent
set autoindent
set tags=./tags;/
filetype plugin on

"visual search
xnoremap * :<C-u>call <SID>VSetSearch()<CR>/<C-R>=@/<CR><CR>
xnoremap # :<C-u>call <SID>VSetSearch()<CR>?<C-R>=@/<CR><CR>
function! s:VSetSearch()
let temp = @s
norm! gv"sy
let @/ = '\V' . substitute(escape(@s, '/\'), '\n', '\\n', 'g')
let @s = temp
endfunction

"Qargs
command! -nargs=0 -bar Qargs execute 'args' QuickfixFilenames()
function! QuickfixFilenames()
let buffer_numbers = {}
for quickfix_item in getqflist()
let buffer_numbers[quickfix_item['bufnr']] = bufname(quickfix_item['bufnr'])
endfor
return join(map(values(buffer_numbers), 'fnameescape(v:val)'))
endfunction

